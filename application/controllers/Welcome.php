<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		// Set the title
        $this->template->title = 'Login';
        
        // Dynamically add a css stylesheet
        //$this->template->stylesheet->add('http://twitter.github.com/bootstrap/assets/css/bootstrap.css');
        
        // Load a view in the content partial
        //$this->template->content->view('hero', array('title' => 'Hello, world!'));

        //$news = array(); // load from model (but using a dummy array here)
        //$this->template->content->view('news', $news);
        
        // Set a partial's content
        //$this->template->footer = 'Made with Twitter Bootstrap';
       
		$data['title'] = 'Login';
		//$this->load->view('login',$data);
		$this->template->set_template('empty');
		$this->template->content->view('login', $data);
        // Publish the template
        $this->template->publish();
	}

	public function login(){
		$data['title'] = 'Dashboard';
		$this->load->view('template',$data);
	}
}
