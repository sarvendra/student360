<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

//To Solve File REST_Controller not found
require (APPPATH . 'libraries/REST_Controller.php');
require (APPPATH . 'libraries/Format.php');


class User extends REST_Controller {

    function __construct(){
        parent::__construct();
    }

    //User Login
    function login_post(){

        $email = $this->post('email'); // param
        $pass = $this->post('password'); // param

        if($email !== NULL && $pass !== NULL){ 
            $this->load->model('user_model'); // load user model
            $user = $this->user_model->get_user_details($email); // get user details
            $pass = md5($pass); // encrypt password
            if(!strcmp($user->password,$pass)){ // compare credentials
                // create response data
                $data = array(
                    'login_status' => 1,
                    'user'=>array(
                        'firstname' => $user->firstname,
                        'lastname' => $user->lastname,
                        'email'=> $user->email,
                        'username'=>$user->username,
                        'mobile'=> $user->mobile
                    ),
                    'success'=>1,
                    'error'=>'',
                    'status' => REST_Controller::HTTP_OK

                );
                $this->set_response($data,REST_Controller::HTTP_OK);
            }else{
                $data = array(
                    'login_status' => 0,
                    'user'=>'',
                    'success'=>0,
                    'error'=>array('message'=>'Invalid email or password'),
                    'status' => REST_Controller::HTTP_NOT_FOUND

                );
                $this->set_response($data,REST_Controller::HTTP_NOT_FOUND);
            }

        }else{
            $data = array(
                'login_status' => 0,
                'user'=>'',
                'success'=>0,
                'error'=>array('message'=>'Invalid request'),
                'status' => REST_Controller::HTTP_BAD_REQUEST

            );
            $this->set_response($data,REST_Controller::HTTP_BAD_REQUEST);
        }    
    }

}