<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class Index extends CI_Controller{

    public function __construct(){
        parent::__construct();
    }

    public function index(){
        // Set the title
        $this->template->title = 'Admin Dashboard';
        
        $data['title'] = 'Admin';
		//$this->template->set_template('empty');
		$this->template->content->view('admin/dashboard', $data);
        // Publish the template
        $this->template->publish();
    }

    public function students(){
        // Set the title
        $this->template->title = 'Student List';
        
        $data['title'] = 'Students';
        $data['title_desc'] = 'Student List';
		$this->template->content->view('admin/student_list', $data);
        // Publish the template
        $this->template->publish();
    }

    public function add_student(){
        $data['title'] = 'Students';
        $data['title_desc'] = 'Add Student';
        // Set the title
        $this->template->title = 'Student';
		$this->template->content->view('admin/add_student', $data);
        // Publish the template
        $this->template->publish();
    }
}