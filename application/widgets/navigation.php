<?php

/*
 * Navigation widget
 */
class Navigation extends Widget {

    public function display($data) {
        $this->view('widgets/navigation');
    }
    
}