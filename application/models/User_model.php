<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model{

    function get_user_details($email){
        try{
            $this->db->select('*');
            $this->db->from('users');
            $this->db->where('email',$email);
            $query = $this->db->get();
            if($query !== FALSE && $query->num_rows() > 0){
                return $query->row();
            }
            return NULL;
        }catch(Exception $er){
            return NULL;
        }
    }

}